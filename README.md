# Lotro mesh converter

A tool to convert 3d models from proprietary lotro format to GLB.

## Usage

The client_mesh.dat file found on your EoA folder first needs to be unpacked.
There are existing tools for this, refer to [lotro-unpacker](https://gitlab.com/lotro/unpacker).

After unpacking, you should have a directory containing all meshes in the format 060XXXXX.bin.
To convert all meshes to GLB format, simply use the tool as follows:

```
lotro-mesh-converter --outdir <where you want the GLB files> <path to mesh directory>
# example:
lotro-mesh-converter --outdir /home/user/exported /home/user/lotro-bin-files
```

## TODO

At the moment, the tool doesn't convert bone data properly. May be added in a later update