#![allow(unused)]

use std::borrow::Cow;
use std::collections::HashMap;
use std::fmt::{Display, format, Formatter};
use std::fs::{File, read};
use std::{fs, io};
use std::io::{BufReader, Read, Seek, Write};
use std::path::PathBuf;
use clap::{arg, Parser};
use clap::builder::Str;
use byteorder::{ByteOrder, ReadBytesExt, BigEndian, LE};
use clap::builder::Resettable::Value;
use gltf::{Glb, json};
use gltf_json;
use gltf_json::{Accessor, Path};
use gltf_json::accessor::sparse::Sparse;
use gltf_json::mesh::{Mode, Semantic};
use gltf_json::validation::Checked::Valid;

#[derive(Parser)]
struct Cli {
    #[arg(short, long, required=true)]
    outdir: PathBuf,
    #[arg(short, long, default_missing_value="false")]
    analyze: bool,
    #[arg(required=true)]
    files: Vec<PathBuf>
}

struct Model {
    pub info1: u16,
    pub info2: u16,
    pub vertex_count: u32,
    unknown_bytes: u8,
    uv_count: u8,
    rigged_val: u8,
    unknown_vertex_data: u8,
    stride: u32,
    rig_section_size: usize,
    pub data: Vec<u8>,
    pub rig_data: Vec<u8>,
    pub index_data: Vec<u8>,
    index_count: u32,
    min: Vec<f32>,
    max: Vec<f32>,
    bone_count: u32,
    bones: Vec<u32>
}

struct EchoesModel {
    id: u32,
    byte1: u16,
    model_count: u32,
    models: Vec<Model>,
    unknown_int: u32,
    byte_length: u32,
    f32_byte_length: u32,
}

impl Default for Model {
    fn default() -> Self {
        Model {
            info1: 0,
            info2: 0,
            vertex_count: 0,
            rigged_val: 0,
            unknown_bytes: 0,
            uv_count: 0,
            index_count: 0,
            rig_section_size: 0,
            data: vec![],
            rig_data: vec![],
            index_data: vec![],
            min: vec![],
            max: vec![],
            bone_count: 0,
            bones: vec![],
            stride: 0,
            unknown_vertex_data: 0
        }
    }
}

trait EchoesRead: io::Read {
    fn read_vec3(&mut self) -> Vec<f32>;
    fn read_u32_vec(&mut self, len: u32) -> Vec<u32>;
}

impl<R: io::Read + ?Sized> EchoesRead for R {
    fn read_vec3(&mut self) -> Vec<f32> {
        Vec::from([
            self.read_f32::<LE>().unwrap(),
            self.read_f32::<LE>().unwrap(),
            self.read_f32::<LE>().unwrap()
        ])
    }

    fn read_u32_vec(&mut self, len: u32) -> Vec<u32> {
        (0..len).map(|_| self.read_u32::<LE>().unwrap()).collect()
    }
}

fn align_to_multiple_of_four(n: &mut u32) {
    *n = (*n + 3) & !3;
}

impl Model {
    pub fn len(&self) -> usize {
        self.data.len() + self.index_data.len()
    }
}

impl EchoesModel {

    fn new(f: &mut File) -> EchoesModel {
        // 4 bytes: file ID
        let id = f.read_u32::<LE>().unwrap();
        // 2 bytes: unknown value.
        let byte1 = f.read_u16::<LE>().unwrap();
        // 2 bytes: 0 if not rigged, 4096 if rigged
        let unknown = f.read_u16::<LE>().unwrap();
        // let rigged = if rigged_val == 4096 { true } else { false };
        // 4 bytes: amount of models contained in file
        let model_count = f.read_u32::<LE>().unwrap();
        // 4 * model_count bytes: model metadata. Unknown what it does
        let mut models: Vec<Model> = (0..model_count).map(|x| Model {
            info1: f.read_u16::<LE>().unwrap(),
            info2: f.read_u16::<LE>().unwrap(),
            ..Default::default()
        }).collect();
        // 4 bytes: unknown int that is usually the same as model_count
        let unknown_int = f.read_u32::<LE>().unwrap();

        // TODO implement non-rigged models (they have different amount of bytes in data field)
        for x in 0..model_count {
            let mut model = &mut models[x as usize];
            // 4 bytes unknown value
            model.unknown_bytes = f.read_u8().unwrap();
            model.uv_count = f.read_u8().unwrap();
            model.rigged_val = f.read_u8().unwrap();
            model.unknown_vertex_data = f.read_u8().unwrap();
            let vertices = f.read_u32::<LE>().unwrap() as usize;
            /*
                By default, assume the following vertex data layout:
                vertex position (3*4=12 bytes)
                vertex normal (3*4=12 bytes)
                vertex tangent (3*4=12 bytes)
                vertex cotangent (3*4=12 bytes)
                total 48 bytes
             */
            model.stride = (48 + model.uv_count * 8) as u32;

            model.rig_section_size = match model.rigged_val {
                0x01 => 5,
                0x02 => 10,
                0x03 => 15,
                0x04 => 20,
                _ => 0
            };
            if model.unknown_vertex_data == 0x00 {
                // Does not contain tangent/cotangent data
                model.stride -= 24;
            }
            model.data = Vec::with_capacity(model.stride as usize * vertices);
            model.rig_data = Vec::with_capacity(model.rig_section_size as usize * vertices);
            for _ in 0..vertices {
                let mut vertex_data = vec![0; model.stride as usize];
                let mut rig_data = vec![0; model.rig_section_size];
                f.read(&mut vertex_data);
                f.read(&mut rig_data);
                model.data.append(&mut vertex_data);
                model.rig_data.append(&mut rig_data);
            }
            model.min = f.read_vec3();
            model.max = f.read_vec3();
            model.bone_count = f.read_u32::<LE>().unwrap();
            model.bones = f.read_u32_vec(model.bone_count);
            model.vertex_count = vertices as u32;
        }

        let index_count = f.read_f32::<LE>().unwrap();
        for x in 0..model_count {
            let mut model = &mut models[x as usize];
            model.index_count = f.read_u32::<LE>().unwrap();
            model.index_data = vec![0; (model.index_count * 2) as usize];
            f.read(&mut model.index_data);
        }
        let byte_length = models.iter().fold(0, |p, m| p + m.len() as u32);
        let f32_byte_length = models.iter().fold(0, |p, m| p + m.data.len() as u32);
        EchoesModel {
            id, byte1,
            model_count,
            models,
            unknown_int,
            byte_length,
            f32_byte_length
        }
    }

    pub fn export(&self, path: &PathBuf) {
        let mut accessors = vec![];
        let mut buffers = vec![];
        let mut buffer_views = vec![];
        let mut meshes = vec![];
        let mut nodes = vec![];
        let mut scenes = vec![json::Scene {
            extensions: Default::default(),
            extras: Default::default(),
            name: None,
            nodes: vec![json::Index::new(0)]
        }];

        let buffer = gltf_json::buffer::Buffer {
            byte_length: self.byte_length,
            extensions: Default::default(),
            extras: Default::default(),
            name: None,
            uri: None
        };
        buffers.push(buffer);

        let mut f32_offset = 0;
        let mut u16_offset = 0;
        for (i, model) in self.models.iter().enumerate() {
            let i = i as u32;
            // TODO implement bones
            // let buffer_view_count = if model.rig_section_size > 0 { 3 } else { 2 };
            let buffer_view_count = 2;
            let accessor_count = 3 + model.uv_count as u32;
            let vertex_view = gltf_json::buffer::View {
                buffer: json::Index::new(0),
                byte_length: model.stride * model.vertex_count,
                byte_offset: Some(f32_offset),
                byte_stride: Some(model.stride),
                extensions: Default::default(),
                extras: Default::default(),
                name: None,
                target: Some(Valid(json::buffer::Target::ArrayBuffer))
            };
            f32_offset += vertex_view.byte_length;
            buffer_views.push(vertex_view);

            let index_view = gltf_json::buffer::View {
                buffer: json::Index::new(0),
                byte_length: model.index_data.len() as u32,
                byte_offset: Some(self.f32_byte_length + u16_offset),
                byte_stride: None,
                extensions: Default::default(),
                extras: Default::default(),
                name: None,
                target: Some(Valid(json::buffer::Target::ElementArrayBuffer))
            };
            u16_offset += index_view.byte_length;
            buffer_views.push(index_view);

            if model.rig_section_size > 0 {
                /* TODO implement bones
                let bone_view = json::buffer::View {
                    buffer: json::Index::new(0),
                    byte_length: model.rig_section_size as u32 * model.vertex_count,
                    byte_offset: Some(data_index),
                    byte_stride: None,
                    extensions: Default::default(),
                    extras: Default::default(),
                    name: None,
                    target: Some(Valid(json::buffer::Target::ArrayBuffer))
                };
                data_index += bone_view.byte_length;
                buffer_views.push(bone_view);
                 */
            }

            let positions = json::Accessor {
                buffer_view: Some(gltf_json::Index::new(i * buffer_view_count as u32)),
                byte_offset: 0,
                count: model.vertex_count,
                component_type: Valid(gltf_json::accessor::GenericComponentType(
                    gltf_json::accessor::ComponentType::F32
                )),
                extensions: Default::default(),
                extras: Default::default(),
                type_: Valid(gltf_json::accessor::Type::Vec3),
                min: Some(gltf_json::Value::from(model.min.clone())),
                max: Some(gltf_json::Value::from(model.max.clone())),
                name: None,
                normalized: false,
                sparse: None
            };
            accessors.push(positions);

            let normals = json::Accessor {
                buffer_view: Some(gltf_json::Index::new(i * buffer_view_count as u32)),
                byte_offset: 12,
                count: model.vertex_count,
                component_type: Valid(json::accessor::GenericComponentType(
                    json::accessor::ComponentType::F32
                )),
                extensions: Default::default(),
                extras: Default::default(),
                type_: Valid(json::accessor::Type::Vec3),
                min: None,
                max: None,
                name: None,
                normalized: false,
                sparse: None
            };
            accessors.push(normals);

            for uv in 0..model.uv_count {
                let uvs = json::Accessor {
                    buffer_view: Some(gltf_json::Index::new(i * buffer_view_count as u32)),
                    byte_offset: (24 + uv * 8) as u32,
                    count: model.vertex_count,
                    component_type: Valid(json::accessor::GenericComponentType(
                        json::accessor::ComponentType::F32
                    )),
                    extensions: Default::default(),
                    extras: Default::default(),
                    type_: Valid(json::accessor::Type::Vec2),
                    min: None,
                    max: None,
                    name: None,
                    normalized: false,
                    sparse: None
                };
                accessors.push(uvs);
            }

            let indices = json::Accessor {
                buffer_view: Some(json::Index::new(i * buffer_view_count as u32 + 1)),
                byte_offset: 0,
                count: model.index_count,
                component_type: Valid(gltf_json::accessor::GenericComponentType(
                    gltf_json::accessor::ComponentType::U16
                )),
                type_: Valid(gltf_json::accessor::Type::Scalar),
                min: None,
                max: None,
                name: None,
                normalized: false,
                sparse: None,
                extensions: Default::default(),
                extras: Default::default()
            };
            accessors.push(indices);
            let primitive = json::mesh::Primitive {
                attributes: {
                    let mut map = HashMap::new();
                    map.insert(
                        Valid(Semantic::Positions),
                        json::Index::new(i * accessor_count)
                    );
                    map.insert(
                        Valid(Semantic::Normals),
                        json::Index::new(i * accessor_count + 1)
                    );
                    for u in 0..model.uv_count {
                        let u = u as u32;
                        map.insert(
                            Valid(Semantic::TexCoords(u as u32)),
                            json::Index::new(i * accessor_count + 2 + u)
                        );
                    }
                    map
                },
                extensions: Default::default(),
                extras: Default::default(),
                indices: Some(json::Index::new(i * accessor_count + 2 + model.uv_count as u32)),
                material: None,
                mode: Valid(Mode::Triangles),
                targets: None
            };

            let mesh = json::Mesh {
                extensions: Default::default(),
                extras: Default::default(),
                name: None,
                primitives: vec![primitive],
                weights: None
            };
            meshes.push(mesh);

            let node = json::Node {
                camera: None,
                children: None,
                extensions: Default::default(),
                extras: Default::default(),
                matrix: None,
                mesh: Some(json::Index::new(0)),
                name: None,
                rotation: None,
                scale: None,
                translation: None,
                skin: None,
                weights: None
            };
            nodes.push(node);
        }

        let root = json::Root {
            accessors, buffers, buffer_views, meshes, nodes, scenes,
            ..Default::default()
        };
        let json_string = json::serialize::to_string(&root).unwrap();
        let mut json_offset = json_string.len() as u32;
        align_to_multiple_of_four(&mut json_offset);
        let total_size_bytes = self.models.iter().fold(0, |_, m| m.data.len());
        let glb = Glb {
            header: gltf::binary::Header {
                magic: *b"glTG",
                version: 2,
                length: json_offset + total_size_bytes as u32,
            },
            json: Cow::Owned(json_string.into_bytes()),
            bin: Some(Cow::Owned(self.models.iter()
                .map(|m| m.data.clone())
                .chain(self.models.iter().map(|m| m.index_data.clone()))
                .flatten()
                .collect()
            ))
        };

        let writer = File::create(path).unwrap();
        glb.to_writer(writer);
    }

    fn open(path: &PathBuf) -> io::Result<EchoesModel> {
        File::open(path).map(|mut f| EchoesModel::new(&mut f))
    }
}

impl Display for EchoesModel {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "id: 0x{:08x}\nshort1: {}\nsections: {}\n",
               self.id, self.byte1, self.model_count
        );
        for sec in &self.models {
            write!(f, "{}", sec);
        }
        writeln!(f, "unknown int: {}", self.unknown_int);
        Ok(())
    }
}

impl Display for Model {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Secinfo: 0x{:04x} 0x{:04x}", self.info1, self.info2);
        writeln!(f, "unknown bytes: 0x{:04x}", self.unknown_bytes);
        writeln!(f, "rigged val: 0x{:02x}", self.rigged_val);
        writeln!(f, "unknown_vertex_data: 0x{:02x}", self.unknown_vertex_data);
        writeln!(f, "bounds: [{}, {}, {}]...[{}, {}, {}]",
                 self.min[0], self.min[1], self.min[2], self.max[0], self.max[1], self.max[2]
        );
        writeln!(f, "bone count: {}", self.bone_count);
        Ok(())
    }
}



fn main() {
    let args: Cli = Cli::parse();
    println!("Exporting {} files...", args.files.len());
    let outdir = &args.outdir;
    let mut files = args.files;
    if files.len() == 1 && files[0].is_dir() {
        let directory = files[0].clone();
        let file_res = fs::read_dir(&directory).unwrap()
            .map(|res| res.map(|e| e.path()))
            .collect::<Result<Vec<_>, io::Error>>();
        files = file_res.unwrap();
    }
    for file in files {
        if let Some(f) = file.extension() {
            if f != "bin" {
                println!("Skipping {}, not a .bin file", file.display());
                continue;
            }
        } else {
            println!("Skipping {}, not a .bin file", file.display());
            continue;
        }
        if let Ok(f) = &mut EchoesModel::open(&file) {
            f.export(&outdir.join(format!("{:08x}.glb", f.id)));
        } else {
            println!("No such file or directory: {}", file.display());
            return;
        }
    }
    println!("Files exported successfully.");
}
